# Some utilities to draw powerset lattices
The final picture generation requires dot ([graphviz](http://www.graphviz.org/content/attrs#dordering))

## Build (with gradle)
```
./gradlew build
```

## Generate a powerset lattice (dot file)
```
groovy \
    -cp build/libs/pwslattice-1.0-SNAPSHOT.jar \
    src/main/groovy/fr/uvsq/adam/pwslattice/genDot.groovy "A,B,C"
```

Without argument, it generates the lattice for "A,B,C,D".

## Generate a powerset lattice with some elements highlighted (dot file)
```
groovy \
    -cp build/libs/pwslattice-1.0-SNAPSHOT.jar \
    src/main/groovy/fr/uvsq/adam/pwslattice/enumVersions.groovy \
    < src/test/resources/test-db.txt
```

## Picture generation
```
dot -Tpng -O fichier.dot
```

Other output formats are supported (see [formats](http://www.graphviz.org/content/output-formats)).

Lattice and picture generation can be merged.

```
groovy \
    -cp build/libs/pwslattice-1.0-SNAPSHOT.jar \
    src/main/groovy/fr/uvsq/adam/pwslattice/enumVersions.groovy \
    < src/test/resources/test-db.txt \
    | dot -Tpng -o test-db.png
```
