package fr.uvsq.adam.pwslattice

import groovy.transform.ToString

@ToString
class PowersetLattice {
    static final String HEADER = '''\
digraph G {
    node [ style=filled ];
    rankdir=BT
    ordering="in"
'''

    static final String FOOTER = '''\
}
'''

    List<String> elements
    Set<Tuple> edges

    PowersetLattice(elements) {
        this.elements = elements
        this.edges = []
    }

    def printHasseDiagram(nodes = []) {
        println HEADER
        printNodes(nodes)
        computeEdges()
        printEdges()
        println FOOTER
    }

    private void printNodes(nodes) {
        nodes.each { bs, label ->
            def bsText = bitsetToString(bs)[1..-2]
            println "    \"" + bsText + "\" [color=red,label=\"" + bsText + "(" + label + ")" + "\"]"
        }
        println()
    }

    private void computeEdges() {
        Set<BitSet> level = createTopLevel()
        while (level) {
            Set<BitSet> nextLevel = generateNextLevelAndEdges(level)
            level = nextLevel
        }
    }

    private Set<BitSet> createTopLevel() {
        final def sz = elements.size()
        Set<BitSet> level = []
        def top = new BitSet(sz)
        top[0..sz-1] = true
        level << top
    }

    private Set<BitSet> generateNextLevelAndEdges(Set<BitSet> level) {
        Set<BitSet> nextLevel = []
        level.each { superset ->
            Set<BitSet> subsets = bitsetSubsets(superset)
            subsets.each { subset ->
                nextLevel << subset
                edges << new Tuple(subset, superset)
            }
        }
        nextLevel
    }

    private void printEdges() {
        edges.sort { t1, t2 ->
            int lhsComparison = bitsetCompare((BitSet)t1[0], (BitSet)t2[0])
            if (lhsComparison == 0) return bitsetCompare((BitSet)t1[1], (BitSet)t2[1])
            return lhsComparison
        }.each {
            println "    " + edgeToString(it)
        }
    }

    private String edgeToString(Tuple edge) {
        return bitsetToString(edge[0]) + " -> " + bitsetToString(edge[1])
    }

    private String bitsetToString(bitset) {
        def str = "\"{ "
        for (int elt = bitset.nextSetBit(0); elt >= 0; elt = bitset.nextSetBit(elt + 1)) {
            str += elements[elt] + ", "
        }
        if (!bitset.isEmpty()) {
            str = str.substring(0, str.size() - 2)
        }
        return str + " }\""
    }

    static Set<BitSet> bitsetSubsets(BitSet superset) {
        Set<BitSet> subsets = []
        for (int elt = superset.nextSetBit(0); elt >= 0; elt = superset.nextSetBit(elt + 1)) {
            BitSet subset = (BitSet)superset.clone()
            subset[elt] = false
            subsets << subset
        }
        subsets
    }

    static int bitsetCompare(BitSet lhs, BitSet rhs) {
        if (lhs.cardinality() < rhs.cardinality()) return -1
        if (lhs.cardinality() > rhs.cardinality()) return 1
        if (lhs == rhs) return 0
        BitSet xorBS = lhs.clone().xor rhs
        int firstDifferent = xorBS.nextSetBit(0)
        assert firstDifferent != -1
        return lhs.get(firstDifferent) ? -1 : 1
    }
}
