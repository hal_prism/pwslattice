#!/usr/bin/env groovy
package fr.uvsq.adam.pwslattice

Set<String> elementNames = []
List<List<String>> db = []

System.in.splitEachLine(",") { elements ->
    db << elements.sort()
    elements.each { elementNames << it }
}

def names = elementNames as List
names.sort()
List<BitSet> bitSets = []

db.each {
    def theSet = new BitSet(names.size())
    it.each { theSet[names.indexOf(it)] = true }
    bitSets << theSet
}

def counts = bitSets.countBy { it }

def lattice = new PowersetLattice(names)
lattice.printHasseDiagram(counts)
