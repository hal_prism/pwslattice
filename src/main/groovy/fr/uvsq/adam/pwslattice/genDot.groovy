#!/usr/bin/env groovy
package fr.uvsq.adam.pwslattice

def param = args.length > 0 ? args[0] : 'A,B,C,D'
def elements = param.split(',')
def lattice = new PowersetLattice(elements)
lattice.printHasseDiagram()
